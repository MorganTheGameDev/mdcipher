# MDCipher

**Created by Morgan Dilling**
(**morgandilling171@gmail.com**)
(**MorganDGamer#6350**)

**INFO**

MDCipher is a cipher that I (Morgan Dilling) have created.

It works fairly simply but produces a fairly complex encrypted message.

If you wish to use it in anything, please make sure to credit me!

**WARNING**

It may pop as a virus by windows defender, I promise you it's not and you can go through all the .py files yourself if you really need to.

-----------------

**HOW TO USE:**

To set the key:
open set key.bat and follow the instructions on screen.

------------------

To encrypt/decrypt:
open crypt.bat

**to encrypt:**

  - type "e" at the d/e option
  - type a message to encrypt

**to decrypt:**

  - type "d" at the d/e option
  - type an encrypted code to decrypt
  - (NOTE: You MUST have the same key for decryption to work properly; otherwise it will still be giberish)

**It will automatically copy outputs to your clipboard!**
