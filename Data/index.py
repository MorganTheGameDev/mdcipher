#Imports#
import time
import pyperclip
import math

#Variables#
alph = [
  "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"," ",",",".",":","!","?"
]
keys = open("Data/key.txt", "r").readlines()
key1 = int(keys[0])
key2 = int(keys[1])
key3 = int(keys[2])

#Functions#
def cal(num, dc):
  num = float(num)
  if dc == "en":
    num = num / key1
    num = num * key2
    return num / key3
  elif dc == "de":
    num = num * key3
    num = num / key2
    return int(num * key1)

def encrypt (letter):
  return cal(alph.index(letter), "en")

def decrypt (num):
  return alph[cal(num, "de")]

def calculate (string, crypt):
  if crypt == "e":
    final = []
    for i in string:
      if i.lower() in alph:
        if i == " ":
          final.append("space")
        elif i == ",":
          final.append("comma")
        elif i == ".":
          final.append("dot")
        elif i == ":":
          final.append("colon")
        elif i == "!":
          final.append("exclamation")
        elif i == "?":
          final.append("question")
        else:
          final.append(str(encrypt(i.lower())))
    return "-".join(final)
  elif crypt == "d":
    final = ""
    tbl = string.split("-")
    for i in tbl:
      if i == "space":
        final += " "
      elif i == "comma":
        final += ","
      elif i == "dot":
        final += "."
      elif i == "colon":
        final += ":"
      elif i == "exclamation":
        final += "!"
      elif i == "question":
        final += "?"
      else:
        final += decrypt(i)
    return final
  else:
    return "Error while calculating"

#Main#
print("NOTE: To paste strings in the console you have to press right click on your mouse.\nAll outputs will be automatically copied to your clipboard.\nOnly some characters outside of the alphabet are able to be used. in order to use numbers use 'one' instead of '1'.")

while True:
  choice = str(input("D/E? ")).lower()
  st = str(input("Enter message: "))

  result = str(calculate(st, choice))

  print("Crypted message: " + result)
  pyperclip.copy(result)